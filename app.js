const express = require('express')
const app = express()
const port = 3000

/**
 * This function multiplies two numbers
 * @param {Number} a first param
 * @param {Number} b second parameter
 * @returns {Number}
 */
const mult = (a,b)=> {
    return a*b
}
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})