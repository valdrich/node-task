# **Node task**

This project contains the files for the assignment : "Node task with Markdown"

---

## Mandatory tasks

### **1 .gitignore**

Is used to block the *node_modules*.

### **2 readme.md**

Is used to build this layout.

### **3 app.js**

Contains a function that given two numbers returs the multiplication between both.

![Forest](Forest.jpeg)